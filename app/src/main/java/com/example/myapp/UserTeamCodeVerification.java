package com.example.myapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;


public class UserTeamCodeVerification extends Fragment {
    TextInputEditText teCodeVerify;
    Button buttonVerify;
    View view;
    Bundle bundle;
    ImageView verifyimage;

    DatabaseReference databaseReference;
    Query query,query2;

    String teamTitle;
    String id;
    String code;

    Fragment fragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPref=getActivity().getPreferences(Context.MODE_PRIVATE);
        id=sharedPref.getString(getString(R.string.id_admin_user),"hellotitle");
        teamTitle=sharedPref.getString(getString(R.string.team_name),"teamname");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_user_team_code_verification, container, false);
        return  view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        verifyimage=view.findViewById(R.id.userverify);
        Glide.with(getContext()).load(R.drawable.ui2).into(verifyimage);
        teCodeVerify=view.findViewById(R.id.codeVerification);

        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/ALGER.TTF");
        ((TextView)view.findViewById(R.id.codeVerify)).setTypeface(font);
        buttonVerify=view.findViewById(R.id.codeVerify);
        buttonVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                code=teCodeVerify.getText().toString();
                databaseReference= FirebaseDatabase.getInstance().getReference();

                //verfy whether the code is correct or not
                query=databaseReference.child("Teams").child(id).child(teamTitle).child("teamVerification");
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        String verification=dataSnapshot.getValue().toString();
                        if(verification.equals(code))
                        {
                            //verifying whether already doned or not
                            query2=databaseReference.child("Teams").child(id).child(teamTitle).child("done");
                            query2.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                       String done=dataSnapshot.getValue().toString();
                                       if(done.equals("0"))
                                       {

                                           Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


                                           NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), "END")
                                                   .setSmallIcon(R.drawable.notification_icon)
                                                   .setContentTitle("Go for a treasure")
                                                   .setContentText("Best Of Luck")
                                                   .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                                   .setSound(soundUri);
                                           ((MainActivity)getActivity()).changeFragment(15);
                                           NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());
                                           notificationManager.notify(0, mBuilder.build());



                                           Toast.makeText(getActivity(),"Team Verified",Toast.LENGTH_SHORT).show();
//                                            int hour=SimpleDateFormat.HOUR_OF_DAY0_FIELD;
//                                           Date startTime= Calendar.getInstance().getTime();

                                           int hour= Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                                           int min= Calendar.getInstance().get(Calendar.MINUTE);
                                           int sec=Calendar.getInstance().get(Calendar.SECOND);
                                           String startTime=hour+":"+min+":"+sec;
                                           databaseReference.child("Teams").child(id).child(teamTitle).child("startTime").setValue(startTime);
                                           databaseReference.child("Teams").child(id).child(teamTitle).child("Score").setValue("0");
                                           ((MainActivity)getActivity()).changeFragment(14);
                                       }
                                       else{
                                           Toast.makeText(getActivity(),"Already doned",Toast.LENGTH_SHORT).show();
                                           ((MainActivity)getActivity()).changeFragment(2);
                                       }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });


                        }
                        else
                        {
                            Toast.makeText(getActivity(),"Wrong Verification Id",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });
    }

}
