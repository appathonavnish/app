package com.example.myapp;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class UserCustomAdapterTeamList extends RecyclerView.Adapter<UserCustomAdapterTeamList.MyViewHolder> {
    Activity activity;
    Context context;
    ArrayList<AddingTeam.Team> userTeamList;
    View view;
    LayoutInflater layoutInflater;
    UserSideTeamList userSideTeamList;

    public UserCustomAdapterTeamList(Activity activity, UserSideTeamList userSideTeamList, ArrayList userTeamList) {
        this.context=context;
        this.activity=activity;
        this.userTeamList=userTeamList;
        layoutInflater= LayoutInflater.from(activity);
        this.userSideTeamList=userSideTeamList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view=layoutInflater.inflate(R.layout.user_team_list,parent,false);
        return  new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.textView.setText(userTeamList.get(position).getteamTitle());
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userSideTeamList.nextFragment(userTeamList.get(position).getteamTitle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return userTeamList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textView;

        public MyViewHolder(View itemView) {
            super(itemView);
            textView=itemView.findViewById(R.id.user_team_name);
        }
    }


}
