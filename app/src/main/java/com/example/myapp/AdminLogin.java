package com.example.myapp;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class AdminLogin extends Fragment {
    TextInputEditText editText1;
    ProgressBar progressBarAminLogin;
    TextInputEditText editText2;
    TextView tvforgot;
    Button button;
    ImageView imageView;
    View view;
    String name;
    String password;
    Button tvCreate;
    FirebaseUser firebaseUser;
    FirebaseAuth firebaseAuth;
    //Inflating the layout
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
    {
       view= inflater.inflate(R.layout.fragment_adminlogin, container, false);
       return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBarAminLogin=view.findViewById(R.id.progress_admin_login);
        progressBarAminLogin.setVisibility(View.INVISIBLE);
        editText1 = view.findViewById(R.id.username);
        editText2 =view.findViewById(R.id.password);

        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/ALGER.TTF");
        ((TextView)view.findViewById(R.id.submit_button)).setTypeface(font);
        button    =view.findViewById(R.id.submit_button);


        ((TextView)view.findViewById(R.id.create_account)).setTypeface(font);

        imageView =view.findViewById(R.id.image_adminlogin);
        tvCreate  =view.findViewById(R.id.create_account);

        tvforgot=view.findViewById(R.id.forgot_password);
        Glide.with(getContext()).load(R.drawable.ui2).into(imageView);


        //on clicking forgot password
        tvforgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).changeFragment(11);
            }
        });


        //Login button click
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                progressBarAminLogin.setVisibility(View.VISIBLE);
                name         =editText1.getText().toString();
                password     =editText2.getText().toString();
                firebaseAuth =FirebaseAuth.getInstance();
                // if fields are empty then toast comes
                if((name.isEmpty() || password.isEmpty()))
                {
                    progressBarAminLogin.setVisibility(View.INVISIBLE);
                    final CustomDialog customDialog =new CustomDialog();
                    customDialog.message("Enter the valid Details", "ok", null, new CustomInterfaceDialog() {
                        @Override
                        public void onPositiveButtonClicked() {
                            customDialog.dismiss();
                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }

                        @Override
                        public void onCancelButtonClicked() {

                        }
                    });
                    customDialog.show(getFragmentManager(),"Dialog");
                }
                //superAdmin Login who enters schools
                else if(name.equals("guptaavnish1807@gmail.com"))
                {
                    if(password.equals("avnish123"))
                    {     ((MainActivity)getActivity()).changeFragment(10); }
                    else
                        Toast.makeText(getActivity(),"Enter the correct password",Toast.LENGTH_SHORT).show();

                }
                //sign in with email
                else
                {
                    firebaseAuth.signInWithEmailAndPassword(name,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                      if(task.isSuccessful())
                      {   firebaseUser=firebaseAuth.getCurrentUser();
                          boolean ebool= firebaseUser.isEmailVerified();
                          //checking whether account is verified or not
                          if(ebool==true)
                          {   progressBarAminLogin.setVisibility(View.INVISIBLE);
                              getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                              ((MainActivity)getActivity()).changeFragment(3);
                          }
                          else
                          {   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                              progressBarAminLogin.setVisibility(View.INVISIBLE);
                              Toast.makeText(getActivity(),"Unverfied Account",Toast.LENGTH_SHORT).show();
                          }
                      }
                      else
                      {
                          progressBarAminLogin.setVisibility(View.INVISIBLE);
                          Toast.makeText(getActivity(),"Wrong Details",Toast.LENGTH_SHORT).show();

                      }
                    }});
                }
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });


        //creating account
        tvCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount();
            }
        });


    }


    public void createAccount()
    {
        ((MainActivity)getActivity()).changeFragment(9);
    }


}
