package com.example.myapp;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class EventFragment extends Fragment {

    View view;
    TextView Gameid;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    Query query,query2;

    Switch aSwitch;
    ProgressBar progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_event, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/airstrike.ttf");
        ((TextView)view.findViewById(R.id.gameid)).setTypeface(font);
        Gameid=view.findViewById(R.id.gameid);
        progressBar=view.findViewById(R.id.progress_event);
        aSwitch=view.findViewById(R.id.event_on_off);
        aSwitch.setVisibility(View.INVISIBLE);

        firebaseAuth=FirebaseAuth.getInstance();
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();
        firebaseUser=firebaseAuth.getCurrentUser();
        query=databaseReference.child("users id").child(firebaseUser.getUid()).child("onValue");

        query2=databaseReference.child("users id").child(firebaseUser.getUid()).child("id");


        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.VISIBLE);
                String value=dataSnapshot.getValue().toString();
                if(value.equals("On"))
                {
                    aSwitch.setChecked(true);
                    aSwitch.setText("Event On");
                }
                else
                {
                    aSwitch.setChecked(false);
                    aSwitch.setText("Event Off");
                }
                progressBar.setVisibility(View.INVISIBLE);
                aSwitch.setVisibility(View.VISIBLE);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        query2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String id=dataSnapshot.getValue().toString();
                Gameid.setText("Game Id "+id);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {    aSwitch.setText("Event On");
                    databaseReference.child("users id").child(firebaseUser.getUid()).child("onValue").setValue("On");

                }
                else
                {
                    aSwitch.setText("Event Off");
                    databaseReference.child("users id").child(firebaseUser.getUid()).child("onValue").setValue("Off");
                }

            }
        });

    }
}
