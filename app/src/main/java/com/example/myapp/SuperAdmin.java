package com.example.myapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;


public class SuperAdmin extends Fragment {

    View view;
    TextInputEditText teCollegeName;
    TextInputEditText teCountValue;
    Button buttonCreateCollege;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    Query query;

    HashMap<String,Object> college1;
    HashMap<String,Object> college2;
    CreateCollege createCollege;
    String collegename,count,value;

    //inflating the layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_super_admin, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        teCollegeName=view.findViewById(R.id.college_name);
        teCountValue=view.findViewById(R.id.count_value);
        buttonCreateCollege=view.findViewById(R.id.create_college);
        //allocating hashmap
        college2=new HashMap<>();
        //allocating class
        createCollege=new CreateCollege();
        //database
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();
        query=databaseReference.child("CollegeIdBegin");

        //on clicking button create college
        buttonCreateCollege.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collegename=teCollegeName.getText().toString();
                count=teCountValue.getText().toString();
                createCollege.setCollegeName(collegename);
                createCollege.setOnCount(count);

                college1=createCollege.mapping();
                //searching the total colleges in database and add it in front of college id
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        //getting the value from database and incrementing it
                        value=dataSnapshot.getValue().toString();
                        int v= Integer.parseInt(value);
                        v=v+1;
                        String val=Integer.toString(v);
                        databaseReference.child("CollegeIdBegin").setValue(val);
                        //value incremented and saved to the database

                        //storing the college created by superadmin in database
                        String id=value+collegename;
                        college1.put("college id",id);
                        college2.put("/All Colleges/"+id+"/",college1);
                        databaseReference.updateChildren(college2).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(getActivity(),"College Created",Toast.LENGTH_SHORT).show();
                                ((MainActivity)getActivity()).changeFragment(2);
                            }
                        });
                        //college stored

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });




            }
        });




    }

    //college details class
    public  static class CreateCollege
    {
        private String collegeName;
        private String onCount;

        public String getCollegeName() {
            return collegeName;
        }

        public void setCollegeName(String collegeName) {
            this.collegeName = collegeName;
        }



        public String getOnCount() {
            return onCount;
        }

        public void setOnCount(String onCount) {
            this.onCount = onCount;
        }
        public HashMap mapping()
        { HashMap<String,Object>college=new HashMap<>();
          college.put("collegeName",collegeName);
          college.put("onCount",onCount);
          return college;

        }



    }
}
