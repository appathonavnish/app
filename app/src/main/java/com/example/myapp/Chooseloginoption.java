package com.example.myapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;


public class Chooseloginoption extends Fragment
{   Button adminButton;
    Button userButton;
    Button backButton;
    View view;
    ImageView imageView;
    final String ChannelId="M_CCC";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.choose_loginoption, container, false);
        adminButton=view.findViewById(R.id.admin);
        backButton=view.findViewById(R.id.mainpage);
        userButton=view.findViewById(R.id.user);
        imageView= view.findViewById(R.id.imagecover);
        Glide.with(getContext()).load(R.drawable.ui2).into(imageView);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {   Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/ALGER.TTF");
        ((TextView)view.findViewById(R.id.admin)).setTypeface(font);
        ((TextView)view.findViewById(R.id.user)).setTypeface(font);

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        super.onViewCreated(view, savedInstanceState);
        adminButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getContext()).changeFragment(0);
            }
        });
        userButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ((MainActivity)getContext()).changeFragment(1);


            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getContext()).changeActivity();
            }
        });
    }
}
