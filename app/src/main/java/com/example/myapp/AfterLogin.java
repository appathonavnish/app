package com.example.myapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class AfterLogin extends Fragment {
    View view;

    Toolbar toolbar;

    ImageView imageView;
    DrawerLayout drawerLayout;
    NavigationView navigationView;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    Query query;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       view= inflater.inflate(R.layout.fragment_after_login, container, false);

       return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbar=(Toolbar)view.findViewById(R.id.tool);
        drawerLayout=view.findViewById(R.id.drawer_layout_admin);
        navigationView=view.findViewById(R.id.navigation_admin);
        imageView=view.findViewById(R.id.afterloginimage);
        Glide.with(getContext()).load(R.drawable.ui2).into(imageView);



        firebaseAuth=FirebaseAuth.getInstance();
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();
        firebaseUser=firebaseAuth.getCurrentUser();
        query=databaseReference.child("users id").child(firebaseUser.getUid()).child("onValue");

        ((MainActivity)getActivity()).setSupportActionBar(toolbar);
        setHasOptionsMenu(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Navigation",Toast.LENGTH_SHORT).show();
                drawerLayout.openDrawer(Gravity.LEFT);

            }
        });


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                drawerLayout.closeDrawers();
                int id=item.getItemId();

                switch (id)
                {
                    case R.id.settings : Toast.makeText(getActivity(),"In progress",Toast.LENGTH_SHORT).show();
                                         break;
                    case R.id.logout    :
                                           CustomDialog customDialog =new CustomDialog();
                                           customDialog.message("Are you sure want to log out", "Log Out", null, new CustomInterfaceDialog() {
                                               @Override
                                               public void onPositiveButtonClicked() {
                                                   ((MainActivity)getActivity()).changeFragment(2);
                                               }

                                               @Override
                                               public void onNegativeButtonClicked() {

                                               }

                                               @Override
                                               public void onCancelButtonClicked() {

                                               }
                                           });
                                           customDialog.show(getFragmentManager(),"Dialog");
                                           firebaseAuth.signOut();
                                           break;
                }

                return true;
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.admin_options,menu);
    }
}

