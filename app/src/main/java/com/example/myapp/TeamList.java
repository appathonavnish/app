package com.example.myapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class TeamList extends Fragment {
    FloatingActionButton floatingActionButton;
    RecyclerView recyclerViewTeam;
    CustomAdapterTeam customAdapterTeam;
    LinearLayoutManager linearLayoutManager;
    FirebaseDatabase firebaseDatabaseTeam;
    DatabaseReference databaseReferenceTeam;
    TextView tvNoTeam;
    ArrayList<AddingTeam.Team> teamList=new ArrayList<>();
    AddingTeam.Team team;
    Toolbar toolbar;
    View view;
    ImageView imageView;
    ProgressBar progressBar;
    int progress=0;
    RelativeLayout relativeLayout;

    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    Query query,mQuery;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
      view= inflater.inflate(R.layout.fragment_team_list, container, false);

    /*  toolbar=view.findViewById(R.id.toolbar_team_list);
        ((MainActivity)getActivity()).setSupportActionBar(toolbar);*/
      progressBar=view.findViewById(R.id.progressteamlist);

      return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
        imageView=view.findViewById(R.id.imageteamlist);
        Glide.with(getContext()).load(R.drawable.ui2).into(imageView);
        recyclerViewTeam =view.findViewById(R.id.recycleviewTeam);
        floatingActionButton=view.findViewById(R.id.floatingbuttonTeam);
        tvNoTeam=view.findViewById(R.id.textnoteam);
        linearLayoutManager =new LinearLayoutManager(getActivity());
        recyclerViewTeam.setLayoutManager(linearLayoutManager);
        firebaseDatabaseTeam=FirebaseDatabase.getInstance();
        databaseReferenceTeam=firebaseDatabaseTeam.getReference();
        relativeLayout=view.findViewById(R.id.relativelayout_tam_list);
        relativeLayout.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        firebaseAuth=FirebaseAuth.getInstance();
        firebaseUser=firebaseAuth.getCurrentUser();
        query=databaseReferenceTeam.child("users id").child(firebaseUser.getUid()).child("id");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String adminID=dataSnapshot.getValue().toString();

                mQuery= databaseReferenceTeam.child("Teams").child(adminID);
                mQuery.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        teamList.clear();
                        for(DataSnapshot mysnapshot:dataSnapshot.getChildren())
                        {
                            progressBar.setProgress(progress+10);
                            team=mysnapshot.getValue(AddingTeam.Team.class);
                            teamList.add(team);
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                        relativeLayout.setVisibility(View.VISIBLE);
                        customAdapterTeam.notifyDataSetChanged();
                        if(!teamList.isEmpty()){
                            recyclerViewTeam.setVisibility(View.VISIBLE);
                            tvNoTeam.setVisibility(View.INVISIBLE);
                        }
                        else
                        {
                            recyclerViewTeam.setVisibility(View.INVISIBLE);
                            tvNoTeam.setVisibility(View.VISIBLE);
                        }

                    }


                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        customAdapterTeam=new CustomAdapterTeam(TeamList.this,getActivity(),teamList);
        recyclerViewTeam.setAdapter(customAdapterTeam);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).changeFragment(7);
            }
        });




    }
    public  void remove(final String title)
    {
        Query query= FirebaseDatabase.getInstance().getReference().child("users id").child(firebaseUser.getUid()).child("id");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String id=dataSnapshot.getValue().toString();
                //        clueList.remove(position);
                FirebaseDatabase.getInstance().getReference().child("Teams").child(id).child(title).removeValue();
                //        customAdapter.notifyItemRemoved(position);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}
