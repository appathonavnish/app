package com.example.myapp;

public interface CustomInterfaceDialog {

    public void onPositiveButtonClicked();
    public void onNegativeButtonClicked();
    public void onCancelButtonClicked();


}
