package com.example.myapp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;


public class AddingClue extends Fragment {

    final static int REQUEST_CHECK_SETTINGS=1;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationCallback mLocationCallback;
    LocationRequest mLocationRequest=new LocationRequest();
    private int MY_PERMISSION_LOCATION=1;

    View view;
    Button button;
    ImageView imageView;
    TextInputEditText textInputEditText;
    TextView textView1;
    TextView textView2;
    Button button2;
    ProgressBar progressBar;

    CustomProgressBar customProgressBar;

    FirebaseDatabase firebaseDatabase ;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    Query query;

    CreateAccount.Data data;
    String key;
    String id1;
    Map<String,Object> map;

    //uses callback for GPS inside oncreate
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocationCallback = new LocationCallback()
        {
            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);

                locationAvailability.isLocationAvailable();
            }

            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                textView1.setText(String.valueOf(locationResult.getLocations().get(0).getLatitude()));
                textView2.setText(String.valueOf(locationResult.getLocations().get(0).getLongitude()));
                mFusedLocationProviderClient.removeLocationUpdates(this);


            }
        };

        createLoactionRequest();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
    }

    //inflating the view
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_adding_clue, container, false);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/ALGER.TTF");
        imageView=view.findViewById(R.id.clueimage);
        Glide.with(getContext()).load(R.drawable.ui2).into(imageView);
        textInputEditText =view.findViewById(R.id.edittextclue);

        textView1 =view.findViewById(R.id.lat);
        textView2=view.findViewById(R.id.lon);


        button=view.findViewById(R.id.locationbutton);
        ((TextView)view.findViewById(R.id.locationbutton)).setTypeface(font);
        button2=view.findViewById(R.id.submit);
        ((TextView)view.findViewById(R.id.submit)).setTypeface(font);

        progressBar=view.findViewById(R.id.progress_clue_list);
        progressBar.setVisibility(View.INVISIBLE);

        customProgressBar=new CustomProgressBar(getActivity(),progressBar);

        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();
        firebaseAuth=FirebaseAuth.getInstance();

        //If fields are empty it shows dialog box
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textInputEditText.getText().toString().isEmpty())
                {
                    final CustomDialog customDialog =new CustomDialog();
                    customDialog.message("Enter all the fields", "ok", null, new CustomInterfaceDialog() {
                        @Override
                        public void onPositiveButtonClicked() {
                            customDialog.dismiss();
                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }

                        @Override
                        public void onCancelButtonClicked() {

                        }
                    });
                    customDialog.show(getFragmentManager(),"Dialog");
                }
                else
                {   customProgressBar.showProgressBar();
                    writeDatabase();
                    customProgressBar.hideProgressBar();
                }


            }
        });

        //Location button click get the permission
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                        (getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.ACCESS_COARSE_LOCATION))
                    {
                        Toast.makeText(getActivity(),"Permission needed",Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_LOCATION);


                    }
                    else{
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_LOCATION);
                    }


                }
                else
                {

                    builderSettingRequest();
                }

            }
        });


    }

    /*public void aftergettingpermission(){
        mFusedLocationProviderClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location!=null)
                {   double lat =location.getLatitude();
                    textView1.setText(String.valueOf(lat));
                    textView2.setText(String.valueOf(location.getLongitude()));
                }

            }
        });

    }*/
    //creating the location Request and builder settings
    public void createLoactionRequest()
    {

        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(50);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }
    protected void builderSettingRequest()
    {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().
                addLocationRequest(mLocationRequest);

        SettingsClient settingsClient = new SettingsClient(getActivity());
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
        task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                startLocation();

            }
        });
        task.addOnFailureListener(getActivity(), new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(getActivity(),
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }

            }
        });
    }

    //start getting the current location and passes the callback
    public void startLocation()
    {
         mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,mLocationCallback,null);

    }

    //on every callback result comes here
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQUEST_CHECK_SETTINGS:
                if (resultCode == Activity.RESULT_OK)
                    startLocation();
                else if(resultCode==Activity.RESULT_CANCELED)
                    Toast.makeText(getActivity(),"GPS Needed",Toast.LENGTH_SHORT).show();
        }
    }





//    class which contains clues data
    public static class Post
    {
        private String clue;
        private String lat,lon;

       /* public Post() {
        }*/

       /* public Post(String clue, String lat, String lon) {
            this.clue = clue;
            this.lat = lat;
            this.lon = lon;

        }
*/
        public String getClue() {
            return clue;
        }

        public void setClue(String clue) {
            this.clue = clue;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLon() {
            return lon;
        }

        public void setLon(String lon) {
            this.lon = lon;
        }

        public HashMap mapping()
        {
            HashMap<String,Object> map = new<String,Object>HashMap();
            map.put("clue",clue);
            map.put("lat",lat);
            map.put("lon",lon);
            return map;
        }
    }

    private void writeDatabase()
    {/*
        Post post = new Post(textInputEditText.getText().toString(),textView1.getText().toString(),textView2.getText().toString());
//        databaseReference.child("DATA").push().setValue(post);

        databaseReference.child("DATA").child(post.getClue()).setValue(post).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(getActivity(),"Clue saved",Toast.LENGTH_SHORT).show();
                ((MainActivity)getActivity()).changeFragment(4);
            }
        });*/
        firebaseUser=firebaseAuth.getCurrentUser();

        query=databaseReference.child("users id").child(firebaseUser.getUid()).child("id");
        query.addValueEventListener(new ValueEventListener() {
           @Override
           public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                 /*map =(Map<String, Object>)  dataSnapshotid.getValue();*/
                      String adminId= dataSnapshot.getValue().toString();
//                       data=dataSnapshotid.getValue(CreateAccount.Data.class);

                       AddingClue.Post post1=new AddingClue.Post();
                       post1.setClue(textInputEditText.getText().toString());
                       post1.setLat(textView1.getText().toString());
                       post1.setLon(textView2.getText().toString());
                       HashMap<String,Object>mapped= post1.mapping();
                       HashMap<String,Object>map=new HashMap<>();
                       map.put("/Clues/"+ adminId+"/"+ post1.getClue(),mapped);
                       databaseReference.updateChildren(map).addOnSuccessListener(new OnSuccessListener<Void>() {
                           @Override
                           public void onSuccess(Void aVoid) {
                               Toast.makeText(getActivity(),"Clue saved",Toast.LENGTH_SHORT).show();
                               customProgressBar.hideProgressBar();
                               ((MainActivity)getActivity()).changeFragment(3);

                           }
                       });
                   }


           @Override
           public void onCancelled(@NonNull DatabaseError databaseError) {

           }


       });

    }





}




