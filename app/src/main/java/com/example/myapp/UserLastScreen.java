package com.example.myapp;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class UserLastScreen extends Fragment {

    View view;
    TextView tvStartTime;
    TextView tvEndTime;
    TextView tvTimeTaken;
    TextView tvScore;
ImageView last;
    AddingTeam.Team team;
    Button buttonHome;

    long diffHours,diffMinutes,diffSeconds;
    DatabaseReference databaseReference;
    Query query,query2;

    SharedPreferences sharedPreferences;
    String id,teamTitle;

     Date startTime,endTime;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences=getActivity().getPreferences(Context.MODE_PRIVATE);
        id=sharedPreferences.getString(getString(R.string.id_admin_user),"null");
        teamTitle=sharedPreferences.getString(getString(R.string.team_name),"null");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view= inflater.inflate(R.layout.fragment_user_last_screen, container, false);
        return  view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvStartTime=view.findViewById(R.id.endscreenstarttime);
        tvEndTime=view.findViewById(R.id.endscreenendtime);
        tvTimeTaken=view.findViewById(R.id.endscreentime_taken);
        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/ALGER.TTF");
        ((TextView)view.findViewById(R.id.taketohome)).setTypeface(font);
        buttonHome=view.findViewById(R.id.taketohome);
        tvScore=view.findViewById(R.id.endscreenscore);

        last=view.findViewById(R.id.imagelast);
        Glide.with(getContext()).load(R.drawable.ui2).into(last);

        databaseReference= FirebaseDatabase.getInstance().getReference();
        query=databaseReference.child("Teams").child(id).child(teamTitle);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                team=dataSnapshot.getValue(AddingTeam.Team.class);
                String srttime=team.getStartTime();
                String endtime=team.getEndTime();
                SimpleDateFormat simpleDateFormat=new SimpleDateFormat("HH:mm:ss");

                Date strt=null;
                Date end=null;
                try {
                    strt=simpleDateFormat.parse(srttime);
                    end=simpleDateFormat.parse(endtime);

                    long difference=end.getTime()-strt.getTime();
                    diffSeconds = difference / 1000 % 60;
                     diffMinutes = difference / (60 * 1000) % 60;
                     diffHours = difference / (60 * 60 * 1000) % 24;
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                /*long st= Long.parseLong(team.getStartTime());
                Date date ;

                Time time= new Time(st);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH/mm/ss");
                date=simpleDateFormat.parse(st);
                time.compareTo()*/
               /* int sec=st/1000;
                int min=sec/60;
                int hr=min/60;*/
               query2=databaseReference.child("Teams").child(id).child(teamTitle).child("Score");
               query2.addListenerForSingleValueEvent(new ValueEventListener() {
                   @Override
                   public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                       String time=diffHours+":"+diffMinutes+":"+diffSeconds;
                       tvStartTime.setText(team.getStartTime());
                       tvEndTime.setText(team.getEndTime());
                       tvTimeTaken.setText(time);
                       tvScore.setText(dataSnapshot.getValue().toString());
                       String done="1";
                       databaseReference.child("Teams").child(id).child(teamTitle).child("done").setValue(done);
                       databaseReference.child("Teams").child(id).child(teamTitle).child("timeTaken").setValue(time);

                       buttonHome.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                               ((MainActivity)getActivity()).changeFragment(2);
                           }
                       });
                   }

                   @Override
                   public void onCancelled(@NonNull DatabaseError databaseError) {

                   }
               });



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
