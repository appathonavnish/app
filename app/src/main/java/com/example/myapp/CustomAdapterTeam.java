package com.example.myapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

public class CustomAdapterTeam extends RecyclerView.Adapter <CustomAdapterTeam.MyViewHolder>{
    ArrayList<AddingTeam.Team>teamList;
    Context context;
    TeamList afterLogin;
    LayoutInflater layoutInflater;
   View view;
   Date date;


    public CustomAdapterTeam(TeamList afterLogin, Context context, ArrayList<AddingTeam.Team> teamList)
    {
        this.teamList=teamList;
        this.context=context;
        this.afterLogin=afterLogin;
        layoutInflater=LayoutInflater.from(context);

    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view=layoutInflater.inflate(R.layout.listofteams,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.textView1.setText(teamList.get(position).getteamTitle());
        holder.textView2.setText(teamList.get(position).getTotalMembers());
        holder.textView3.setText(teamList.get(position).getTeamVerification());
        holder.tvStartTime.setText(teamList.get(position).getStartTime());
        holder.tvEndTime.setText(teamList.get(position).getEndTime());
        holder.tvTimeTaken.setText(teamList.get(position).getTimeTaken());
        holder.imageViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                afterLogin.remove(teamList.get(position).getteamTitle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return teamList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView textView1;
        TextView textView2;
        TextView textView3;
        TextView tvStartTime;
        TextView tvEndTime;
        TextView tvTimeTaken;
        ImageView imageViewCancel;
        public MyViewHolder(View itemView) {
            super(itemView);
            textView1=itemView.findViewById(R.id.texttitle);
            textView2=itemView.findViewById(R.id.textmember);
            textView3=itemView.findViewById(R.id.textverify);
            tvStartTime=itemView.findViewById(R.id.start_time);
            tvEndTime=itemView.findViewById(R.id.end_time);
            tvTimeTaken=itemView.findViewById(R.id.time_taken);
            imageViewCancel=itemView.findViewById(R.id.teamcancel);
        }
    }

}
