package com.example.myapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;


public class UserQuestions extends Fragment {

    View view;
    TextView tvQuestion;
    Button buttonCheck;
    ImageView question;

    ArrayList<AddingClue.Post> cluesList;
    Bundle bundle;
    SharedPreferences sharedPreferences;

    Random ran;
    int position;
    String id,teamTitle;
    int listSize;
    int count;
    int cluesolved[],i;
    String latitude,longitude;
    static int MY_PERMISSION_LOCATION =1;
    final static int REQUEST_CHECK_SETTINGS=1;

    LocationCallback mLocationCallback;
    FusedLocationProviderClient mFusedLocationProviderClient;
    LocationRequest mLocationRequest=new LocationRequest();


    DatabaseReference databaseReference;
    Query query,query1,query2;

    AddingClue.Post clue;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences=getActivity().getPreferences(Context.MODE_PRIVATE);
        id=sharedPreferences.getString(getString(R.string.id_admin_user),"null");
        teamTitle=sharedPreferences.getString(getString(R.string.team_name),"null");
        mLocationCallback=new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
//                Location location =new Location("person");


                Location location= new Location(locationResult.getLocations().get(0));

                location.setLatitude(locationResult.getLocations().get(0).getLatitude());
                location.setLongitude(locationResult.getLocations().get(0).getLongitude());

//                Location location1 = Location.

                float[] result={0.0f,0.0f};

                Location.distanceBetween(location.getLatitude(),location.getLongitude(),Double.valueOf(latitude),Double.valueOf(longitude),result);

                /* Location location1=new Location("Clue");
                location1.setLatitude(Double.valueOf(latitude));
                location1.setLongitude(Double.valueOf(longitude));

                double distance=location.distanceTo(location1);*/
               count++;
               if(result[0]<=20)
                {
                    tvQuestion.setVisibility(View.INVISIBLE);
                    position=ran.nextInt(listSize)+0;
                    query1=databaseReference.child("Teams").child(id).child(teamTitle).child("Score");
                    query1.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            int score=Integer.parseInt(dataSnapshot.getValue().toString());
                            score=score+1;
                            Toast.makeText(getActivity(),"Score is"+score,Toast.LENGTH_SHORT).show();
                            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);


                            databaseReference.child("Teams").child(id).child(teamTitle).child("Score").setValue(Integer.toString(score));
                            if(i==listSize)
                            {

                                Toast.makeText(getActivity(),"Event Complete",Toast.LENGTH_SHORT).show();
                                int hour= Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                                int min= Calendar.getInstance().get(Calendar.MINUTE);
                                int sec=Calendar.getInstance().get(Calendar.SECOND);
                                String endTime=hour+":"+min+":"+sec;
                                databaseReference.child("Teams").child(id).child(teamTitle).child("endTime").setValue(endTime);
                                ((MainActivity)getActivity()).changeFragment(15);

                            }
                            else {

                                position=ran.nextInt(listSize)+0;
                                showClue();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }
                else {
                   if(count==3)
                   {
                       Toast.makeText(getActivity(),"You attempted all 3 choices",Toast.LENGTH_SHORT).show();
                       mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);

                       if(i==listSize)
                       {
                           tvQuestion.setVisibility(View.INVISIBLE);
                           Toast.makeText(getActivity(),"Event Complete",Toast.LENGTH_SHORT).show();
                           int hour= Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
                           int min= Calendar.getInstance().get(Calendar.MINUTE);
                           int sec=Calendar.getInstance().get(Calendar.SECOND);
                           String endTime=hour+":"+min+":"+sec;
                           databaseReference.child("Teams").child(id).child(teamTitle).child("endTime").setValue(endTime);

                           Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


                           NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), "END")
                                   .setSmallIcon(R.drawable.notification_icon)
                                   .setContentTitle("Finish")
                                   .setContentText("Thanks for participating")
                                   .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                   .setSound(soundUri);
                           ((MainActivity)getActivity()).changeFragment(15);
                           NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());
                           notificationManager.notify(1, mBuilder.build());



                       }
                       else
                       {
                           position=ran.nextInt(listSize)+0;
                           showClue();
                       }


                   }
                   else {
                       Toast.makeText(getActivity(), "Wrong answer", Toast.LENGTH_SHORT).show();
                       mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
                   }

               }
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);

            }
        };
        createLoactionRequest();
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      view= inflater.inflate(R.layout.fragment_user_questions, container, false);
      return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvQuestion=view.findViewById(R.id.clue_shown);
        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/ALGER.TTF");
        ((TextView)view.findViewById(R.id.check_answer)).setTypeface(font);
        buttonCheck=view.findViewById(R.id.check_answer);

        question=view.findViewById(R.id.imagequestion);
        Glide.with(getContext()).load(R.drawable.ui2).into(question);


        buttonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builderSettingRequest();
            }
        });

         cluesList=new ArrayList<>();


        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION))
            {
                Toast.makeText(getActivity(),"Permission needed",Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_LOCATION);


            }
            else{
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_LOCATION);
            }


        }


        databaseReference= FirebaseDatabase.getInstance().getReference();
        query=databaseReference.child("Clues").child(id);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot data:dataSnapshot.getChildren())
                {
                    clue=data.getValue(AddingClue.Post.class);
                    cluesList.add(clue);

                }

                listSize=cluesList.size();
                cluesolved=new int[listSize];
                Arrays.fill(cluesolved,-1);
                i=0;

                ran=new Random();
                position=ran.nextInt(listSize)+0;
                showClue();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }



    public void showClue()
    {   count=0;
        tvQuestion.setVisibility(View.INVISIBLE);
        int flag=0;
        if(i<listSize)
        {
            int j=0;
            while(j<listSize)
            {

                if(cluesolved[j]!=position)
                {
                    j++;
                    flag=0;
                }
                else
                {
                    j++;
                  flag=1;
                  break;
                }

            }
            if(flag==0)
            {
                cluesolved[i]=position;
                i++;
                tvQuestion.setVisibility(View.VISIBLE);
                tvQuestion.setText(cluesList.get(position).getClue());
                latitude=cluesList.get(position).getLat();
                longitude=cluesList.get(position).getLon();



            }
            else
            {
                position=ran.nextInt(listSize)+0;
                showClue();
            }
        }


    }

   /* private class MyLocationListner implements LocationListener{

        @Override
        public void onLocationChanged(Location location) {
            Location location1=new Location("Clue");
            location1.setLatitude(Double.valueOf(latitude));
            location1.setLongitude(Double.valueOf(longitude));

            double distance=location.distanceTo(location1);
            if(distance<=20)
            {
                position=ran.nextInt(listSize)+0;
                showClue();
            }
        }
    }*/



    public void createLoactionRequest()
    {

        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(50);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }



    protected void builderSettingRequest()
    {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().
                addLocationRequest(mLocationRequest);

        SettingsClient settingsClient = new SettingsClient(getActivity());
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());
        task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                startLocation();

            }
        });
        task.addOnFailureListener(getActivity(), new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(getActivity(),REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException sendEx) {
                        Toast.makeText(getActivity(),"hello",Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    //start getting the current location and passes the callback
    public void startLocation()
    {
        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,mLocationCallback,null);

    }

    //on selecting permission result comes here
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQUEST_CHECK_SETTINGS:
                if (resultCode == Activity.RESULT_OK)
                    startLocation();
                else if(resultCode==Activity.RESULT_CANCELED)
                    Toast.makeText(getActivity(),"GPS needed to find answer",Toast.LENGTH_SHORT).show();
        }
    }




}
