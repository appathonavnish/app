package com.example.myapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ar.app.UnityPlayerActivity;
import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.lang.reflect.Type;

public class Main2Activity extends AppCompatActivity {

    Button treasure;
    Button ar;
ImageView main;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
main=findViewById(R.id.main2);
        Typeface font= Typeface.createFromAsset(getAssets(),"font/ALGER.TTF");
        ((TextView)findViewById(R.id.treasurehunt)).setTypeface(font);
        ((TextView)findViewById(R.id.ar)).setTypeface(font);

        Glide.with(getApplicationContext()).load(R.drawable.ui2).into(main);
        treasure= findViewById(R.id.treasurehunt);
        ar=findViewById(R.id.ar);


        treasure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth=FirebaseAuth.getInstance();
        firebaseUser=firebaseAuth.getCurrentUser();
        if(firebaseUser !=null) {
            Intent intent = new Intent(Main2Activity.this, MainActivity.class);
            intent.putExtra("check", 1);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(Main2Activity.this, MainActivity.class);
            intent.putExtra("check", 0);
            startActivity(intent);
            finish();
        }

            }
        });

       ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main2Activity.this, UnityPlayerActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
