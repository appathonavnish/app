package com.example.myapp;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;


public class CreateAccount extends Fragment {
    View view;
    TextInputEditText tEditFirst;
    TextInputEditText tEditLast;
    TextInputEditText tEditEmail;
    TextInputEditText tEditPassword;
    TextInputEditText tEditConfirmPassword;
    TextInputEditText tEditOnValue;
    Button buttonCreateAccount;
    ProgressBar progressBarCreateAccount;
    ImageView ivCreateAccount;
    TextInputEditText teCollegeId;


    FirebaseAuth userAuth;
    FirebaseUser firebaseUser;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    Query query,query2;

    Data data;
    HashMap<String,Object>hashed;
    HashMap<String,Object>hash;


    String count,id;
    String first,last,email,password,confirmPassword,collegeId,onValue;


    //inflating the layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       view=inflater.inflate(R.layout.fragment_create_account, container, false);
       return  view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        data=new Data();
        hashed=new HashMap<>();

        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/ALGER.TTF");
        ((TextView)view.findViewById(R.id.create_account_button)).setTypeface(font);
        //layout mapping by id
        ivCreateAccount=view.findViewById(R.id.createaccountimage);
        tEditFirst=view.findViewById(R.id.firstnameinput);
        tEditLast=view.findViewById(R.id.lastnameinput);
        tEditEmail=view.findViewById(R.id.emailinput);
        tEditPassword=view.findViewById(R.id.passwordinput);
        tEditConfirmPassword=view.findViewById(R.id.condirmpasswordinput);
        buttonCreateAccount=view.findViewById(R.id.create_account_button);
        progressBarCreateAccount=view.findViewById(R.id.progress_create_account);
        teCollegeId=view.findViewById(R.id.college_id);
        tEditOnValue=view.findViewById(R.id.on_value);
        progressBarCreateAccount.setVisibility(View.INVISIBLE);
        Glide.with(getContext()).load(R.drawable.ui2).into(ivCreateAccount);


        //firebase content
        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();
        userAuth=FirebaseAuth.getInstance();

        //clicking create account button
        buttonCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBarCreateAccount.setVisibility(View.VISIBLE);
                first=tEditFirst.getText().toString();
                last=tEditLast.getText().toString();
                email=tEditEmail.getText().toString();
                password=tEditPassword.getText().toString();
                confirmPassword=tEditConfirmPassword.getText().toString();
                collegeId=teCollegeId.getText().toString();
                onValue=tEditOnValue.getText().toString();
                //query the database to search the college id to validate whether college exists or not
                query=databaseReference.child("All Colleges").child(collegeId);

                //initialising the HashMap
                data.setFirstname(first);
                data.setId(collegeId);
                data.setOnValue(onValue);

                //checking any field empty
                if(first.isEmpty()||last.isEmpty()||email.isEmpty()||password.isEmpty()||confirmPassword.isEmpty()||collegeId.isEmpty())
                {  progressBarCreateAccount.setVisibility(View.INVISIBLE);
                   final CustomDialog customDialog =new CustomDialog();
                   customDialog.message("Enter all the fields", "Ok", null, new CustomInterfaceDialog() {
                       @Override
                       public void onPositiveButtonClicked() {
                             customDialog.dismiss();
                       }
                       @Override
                       public void onNegativeButtonClicked() {

                       }
                       @Override
                       public void onCancelButtonClicked() {

                       }
                   });
                   customDialog.show(getFragmentManager(),"Dailog");
                }
                //checking whether the school id exists or not
                if(query==null)
                {    progressBarCreateAccount.setVisibility(View.INVISIBLE);
                     Toast.makeText(getActivity(),"College ID doesnot exist",Toast.LENGTH_SHORT).show();
                    ((MainActivity)getActivity()).changeFragment(9);
                }

                //if query exists checking whether the passwords are same or not
                else
                {   //whether the password entered is same or not
                    if (!password.equals(confirmPassword))
                    {
                        final CustomDialog customDialog = new CustomDialog();
                        customDialog.message("Ok", "Password not matched", null, new CustomInterfaceDialog() {
                            @Override
                            public void onPositiveButtonClicked() {
                                customDialog.dismiss();
                            }

                            @Override
                            public void onNegativeButtonClicked() {

                            }

                            @Override
                            public void onCancelButtonClicked() {

                            }
                        });
                        customDialog.show(getFragmentManager(), "Dailog");

                    }
                    //if password is same and college id exists then this is performed
                    else
                    {   getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        //creating email using firebase Authentication API
                        try {
                            userAuth.createUserWithEmailAndPassword(tEditEmail.getText().toString(), tEditPassword.getText().toString())
                                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            //if account created successfully
                                            if (task.isSuccessful()) {
                                                firebaseUser = userAuth.getCurrentUser();

                                                //adding listner to above query
                                                query.addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                                        //getting the count of the college id and increment it
                                                        count = dataSnapshot.child("onCount").getValue().toString();
                                                        int c = Integer.parseInt(count);
                                                        c = c + 1;
                                                        String upcount = Integer.toString(c);
                                                        databaseReference.child("All Colleges").child(collegeId).child("onCount").setValue(upcount);
                                                        //no. of ids per college updated
                                                        id = dataSnapshot.child("college id").getValue().toString();

                                                        //making admin id on the basis of college id and count increment
                                                        data.setUid(firebaseUser.getUid());
                                                        data.setId(id + count);
                                                        hash = data.mapping();
                                                        hashed.put("/users id/" +firebaseUser.getUid()+ "/", hash);
                                                        final CustomDialog customDialog =new CustomDialog();
                                                        customDialog.message(id+count, "ok", null, new CustomInterfaceDialog() {
                                                            @Override
                                                            public void onPositiveButtonClicked() {
                                                                customDialog.dismiss();
                                                            }

                                                            @Override
                                                            public void onNegativeButtonClicked() {

                                                            }

                                                            @Override
                                                            public void onCancelButtonClicked() {

                                                            }
                                                        });
                                                        customDialog.show(getFragmentManager(),"Dialog");
                                                        databaseReference.updateChildren(hashed).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                            @Override
                                                            public void onSuccess(Void aVoid) {

                                                            }
                                                        });
                                                        progressBarCreateAccount.setVisibility(View.INVISIBLE);
                                                        Toast.makeText(getActivity(), "Sending verfication email", Toast.LENGTH_SHORT).show();
                                                    /*UserProfileChangeRequest.Builder userProfileChangeRequest = new UserProfileChangeRequest.Builder();
                                                    userProfileChangeRequest.setDisplayName(first);
                                                    firebaseUser.updateProfile(userProfileChangeRequest.build());*/
                                                        progressBarCreateAccount.setVisibility(View.VISIBLE);

                                                        firebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()) {
                                                                    progressBarCreateAccount.setVisibility(View.INVISIBLE);
                                                                    Toast.makeText(getActivity(), "Verification email sent", Toast.LENGTH_SHORT).show();
                                                                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                                    ((MainActivity) getActivity()).changeFragment(0);

                                                                } else {
                                                                    progressBarCreateAccount.setVisibility(View.INVISIBLE);
                                                                    Toast.makeText(getActivity(), "Retry again create account", Toast.LENGTH_SHORT).show();
                                                                    ((MainActivity) getActivity()).changeFragment(9);
                                                                }
                                                            }
                                                        });

                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });

                                            } else {
                                                progressBarCreateAccount.setVisibility(View.INVISIBLE);
                                                Toast.makeText(getActivity(), "Unable to create Account", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                        }catch (Exception e){ Toast.makeText(getActivity(),"Enter the values",Toast.LENGTH_SHORT).show();
                                              getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);    }
                    }
                }
                progressBarCreateAccount.setVisibility(View.INVISIBLE);


            }
        });

    }


    public static class Data
    {
        private String id;
        private String onValue;
        private String uid;
        private String firstname;

        public String getOnValue() {
            return onValue;
        }
        public void setOnValue(String onValue) {
            this.onValue = onValue;
        }
        public String getUid() {
            return uid;
        }
        public void setUid(String uid) {
            this.uid = uid;
        }
        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
        public String getFirstname() {
            return firstname;
        }
        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public HashMap mapping()
        {
            HashMap<String,Object> hash= new HashMap<>();
            hash.put("id",id);
            hash.put("firstname",firstname);
            hash.put("uid",uid);
            hash.put("onValue",onValue);
            return hash;
        }
    }


}
