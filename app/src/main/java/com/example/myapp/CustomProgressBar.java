package com.example.myapp;

import android.app.Activity;
import android.view.WindowManager;
import android.widget.ProgressBar;

public class CustomProgressBar extends ProgressBar {
   Activity activity;
   ProgressBar progressBar;
    public CustomProgressBar(Activity activity,ProgressBar progressBar) {
        super(activity);
        this.activity=activity;
        this.progressBar=progressBar;

    }
    public  void showProgressBar()
    {  activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
       progressBar.setVisibility(VISIBLE);

    }

    public void hideProgressBar()
    {  activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressBar.setVisibility(INVISIBLE);
    }


}
