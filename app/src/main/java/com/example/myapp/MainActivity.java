package com.example.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    Fragment fragment = null;
    AdminLogin adminLogin;
    user user;
    AfterLogin afterLogin;
    ClueList clueList;
    LeaderBoard leaderBoard;
    AddingClue addingClue;
    AddingTeam addingTeam;
    TeamList teamList;
    FragmentTransaction ft;
    FragmentManager fm;
    Chooseloginoption chooseloginoption;
    CreateAccount createAccount;
    SuperAdmin superAdmin;
    ForgotPassword forgotPassword;
    UserSideTeamList userSideTeamList;
    UserTeamCodeVerification userTeamCodeVerification;
    UserQuestions userQuestions;
    UserLastScreen userLastScreen;


    FirebaseUser firebaseUser;
    FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        int check = intent.getIntExtra("check", 0);
        if (check == 1) {
            changeFragment(3);
        }


    }

    public void changeFragment(int id) {
        ft = getSupportFragmentManager().beginTransaction();
        switch (id) {
            case 0:
                if (adminLogin != null)

                    ft.replace(R.id.fragment_main, adminLogin);
                else {
                    adminLogin = new AdminLogin();
                    ft.replace(R.id.fragment_main, adminLogin);
                }
                ft.addToBackStack("name");
                ft.commit();
                break;
            case 1:
                if (user != null)

                    ft.replace(R.id.fragment_main, user);
                else {
                    user = new user();
                    ft.replace(R.id.fragment_main, user);
                }
                ft.addToBackStack("name2");
                ft.commit();
                break;
            case 2:
                getSupportFragmentManager().popBackStack();
                if (chooseloginoption != null)
                    ft.replace(R.id.fragment_main, chooseloginoption);
                else {
                    chooseloginoption = new Chooseloginoption();
                    ft.replace(R.id.fragment_main, chooseloginoption);
                }
                ft.commit();
                break;
            case 3:
                getSupportFragmentManager().popBackStack();
                if (afterLogin != null) {
//                        Toast.makeText(getApplicationContext(), "Press again to log out", Toast.LENGTH_SHORT).show();
                    ft.replace(R.id.fragment_main, afterLogin);
                } else {
                    afterLogin = new AfterLogin();
                    ft.replace(R.id.fragment_main, afterLogin);
                }


                ft.commit();
                break;
            case 4:
                if (clueList != null)
                    ft.replace(R.id.fragment_main, clueList);
                else {
                    clueList = new ClueList();
                    ft.replace(R.id.fragment_main, clueList);
                }
                ft.addToBackStack("name4");
                ft.commit();
                break;
            case 5:
                if (leaderBoard != null)
                    ft.replace(R.id.fragment_main, leaderBoard);
                else {
                    leaderBoard = new LeaderBoard();
                    ft.replace(R.id.fragment_main, leaderBoard);
                }
                ft.addToBackStack("name5");
                ft.commit();
                break;
            case 6:
                getSupportFragmentManager().popBackStack();
                if (addingClue != null)
                    ft.replace(R.id.fragment_main, addingClue);
                else {
                    addingClue = new AddingClue();
                    ft.replace(R.id.fragment_main, addingClue);
                }
                ft.addToBackStack("name6");
                ft.commit();
                break;
            case 7:
                getSupportFragmentManager().popBackStack();
                if (addingTeam != null)
                    ft.replace(R.id.fragment_main, addingTeam);
                else {
                    addingTeam = new AddingTeam();
                    ft.replace(R.id.fragment_main, addingTeam);
                }
                ft.addToBackStack("name7");
                ft.commit();
                break;
            case 8:
                if (teamList != null)
                    ft.replace(R.id.fragment_main, teamList);
                else {
                    teamList = new TeamList();
                    ft.replace(R.id.fragment_main, teamList);
                }
                ft.addToBackStack("name8");
                ft.commit();
                break;
            case 9:
                if (createAccount != null)
                    ft.replace(R.id.fragment_main, createAccount);
                else {
                    createAccount = new CreateAccount();
                    ft.replace(R.id.fragment_main, createAccount);
                }
                ft.addToBackStack("name9");
                ft.commit();
                break;
            case 10:
                getSupportFragmentManager().popBackStack();
                if (superAdmin != null)
                    ft.replace(R.id.fragment_main, superAdmin);
                else {
                    superAdmin = new SuperAdmin();
                    ft.replace(R.id.fragment_main, superAdmin);
                }
                ft.addToBackStack("name10");
                ft.commit();
                break;
            case 11:
                if (forgotPassword != null)
                    ft.replace(R.id.fragment_main, forgotPassword);
                else {
                    forgotPassword = new ForgotPassword();
                    ft.replace(R.id.fragment_main, forgotPassword);
                }
                ft.addToBackStack("name11");
                ft.commit();
                break;
            case 12:
                if (userSideTeamList != null)
                    ft.replace(R.id.fragment_main, userSideTeamList);
                else {
                    userSideTeamList = new UserSideTeamList();
                    ft.replace(R.id.fragment_main, userSideTeamList);
                }
                ft.addToBackStack("name12");
                ft.commit();
                break;
            case 13:
                getSupportFragmentManager().popBackStack();
                if (userTeamCodeVerification != null)
                    ft.replace(R.id.fragment_main, userTeamCodeVerification);
                else {
                    userTeamCodeVerification = new UserTeamCodeVerification();
                    ft.replace(R.id.fragment_main, userTeamCodeVerification);
                }
                ft.addToBackStack("name13");
                ft.commit();
                break;
            case 14:
                if (userQuestions != null)
                    ft.replace(R.id.fragment_main, userQuestions);
                else {
                    userQuestions = new UserQuestions();
                    ft.replace(R.id.fragment_main, userQuestions);
                }
                ft.commit();
                break;
            case 15:
                getSupportFragmentManager().popBackStack();
                if (userLastScreen != null)
                    ft.replace(R.id.fragment_main, userLastScreen);
                else {
                    userLastScreen = new UserLastScreen();
                    ft.replace(R.id.fragment_main, userLastScreen);
                }
                ft.commit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment frg = getSupportFragmentManager().findFragmentById(R.id.fragment_main);
        if (frg != null) {
            frg.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void changeActivity()
    {
        Intent i= new Intent(MainActivity.this,Main2Activity.class);
        startActivity(i);
        finish();
    }



}
