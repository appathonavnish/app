package com.example.myapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class ClueList extends Fragment
{  //all views object
    View view;
    RecyclerView recyclerView;
    ImageView imageView;
    ProgressBar progressBar;
    TextView tvVisible;
    FloatingActionButton floatingActionButton;
    LinearLayoutManager linearLayoutManager;
    RelativeLayout relativeLayout;
    android.support.v7.widget.Toolbar toolbar;

    // all Firebase Object
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;
    Query mQuery;
    Query query2;

    AddingClue.Post post;
    ArrayList<AddingClue.Post> clueList=new ArrayList<>();
    CustomAdapterClue customAdapter;


    int progress=0;

    //inflating the layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_clue_list, container, false);
      /*  toolbar=view.findViewById(R.id.toolbar_clue_list);
        ((MainActivity)getActivity()).setSupportActionBar(toolbar);*/

        return view;

    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recycleview1);
        imageView = view.findViewById(R.id.cluelistimage);
        floatingActionButton = view.findViewById(R.id.floatingbutton);
        tvVisible = view.findViewById(R.id.noclue);
        progressBar = view.findViewById(R.id.progresscluelist);
        relativeLayout=view.findViewById(R.id.relative_layout_clue_list);
      //  Glide.with(getActivity()).load(R.drawable.ui).into(imageView);

        firebaseAuth=FirebaseAuth.getInstance();
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        firebaseUser=firebaseAuth.getCurrentUser();

        setHasOptionsMenu(true);

       /* setHasOptionsMenu(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();*/
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).changeFragment(6);
            }
        });

        relativeLayout.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);

        //query to search the database to et admin id to store the clues in that particular id
        query2=databaseReference.child("users id").child(firebaseUser.getUid()).child("id");
        query2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 String adminId=dataSnapshot.getValue().toString();

                 //query to search the clue list of particular id
                 mQuery = databaseReference.child("Clues").child(adminId);
                 mQuery.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                        clueList.clear();
                        for (DataSnapshot mysnap : dataSnapshot.getChildren()) {

                            post = mysnap.getValue(AddingClue.Post.class);
                            progress = progress + 10;
                            progressBar.setProgress(progress);
                            clueList.add(post);
                        }

                        progressBar.setVisibility(View.INVISIBLE);
                        relativeLayout.setVisibility(View.VISIBLE);
                        customAdapter.notifyDataSetChanged();
                        if (!clueList.isEmpty()) {
                            recyclerView.setVisibility(View.VISIBLE);
                            tvVisible.setVisibility(View.INVISIBLE);

                        } else {
                            recyclerView.setVisibility(View.INVISIBLE);
                            tvVisible.setVisibility(View.VISIBLE);
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        customAdapter = new CustomAdapterClue(this, getActivity(), clueList);
        recyclerView.setAdapter(customAdapter);



    }

    //removing the the on clicking the cancel button
    public void remove(final String clue, int position){
        Query query= FirebaseDatabase.getInstance().getReference().child("users id").child(firebaseUser.getUid()).child("id");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String id=dataSnapshot.getValue().toString();
                //        clueList.remove(position);
                FirebaseDatabase.getInstance().getReference().child("Clues").child(id).child(clue).removeValue();
                //        customAdapter.notifyItemRemoved(position);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    //menu for toolbar
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}
