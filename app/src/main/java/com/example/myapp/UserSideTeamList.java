package com.example.myapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class UserSideTeamList extends Fragment {

    View view;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    ImageView userteam;

    UserCustomAdapterTeamList userCustomAdapterTeamList;

    AddingTeam.Team team;
    ArrayList<AddingTeam.Team> userTeamList;
    String id1;

    DatabaseReference databaseReference;
    Query query;

    SharedPreferences sharedPreferences;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences=getActivity().getPreferences(Context.MODE_PRIVATE);
       id1=sharedPreferences.getString(getString(R.string.id_admin_user),"hello");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_user_side_team_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        linearLayoutManager=new LinearLayoutManager(getActivity());

        userteam=view.findViewById(R.id.imageteam);
        Glide.with(getContext()).load(R.drawable.ui2).into(userteam);

        recyclerView=view.findViewById(R.id.recyclerview_user_team_list);
        recyclerView.setLayoutManager(linearLayoutManager);
        team=new AddingTeam.Team();
        userTeamList = new ArrayList<>();



        databaseReference= FirebaseDatabase.getInstance().getReference();
        query=databaseReference.child("Teams").child(id1);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot2:dataSnapshot.getChildren())
                {
                   team=dataSnapshot2.getValue(AddingTeam.Team.class);
                   userTeamList.add(team);
                }
                userCustomAdapterTeamList.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        userCustomAdapterTeamList=new UserCustomAdapterTeamList(getActivity(),UserSideTeamList.this,userTeamList);
        recyclerView.setAdapter(userCustomAdapterTeamList);

    }

    public void nextFragment(String teamTitle)
    {
         SharedPreferences.Editor editor=sharedPreferences.edit();
         editor.putString(getString(R.string.team_name),teamTitle);
         editor.commit();
        ((MainActivity)getActivity()).changeFragment(13);
    }


}
