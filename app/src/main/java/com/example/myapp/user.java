package com.example.myapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class user extends Fragment {

   View view;
   TextInputEditText teGameId;
   Button buttonStart;
   ImageView user;
ProgressBar progress;
   FirebaseDatabase firebaseDatabase;
   DatabaseReference databaseReference;
   Query query;

   String id;
   CreateAccount.Data dataSearchId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {

        view= inflater.inflate(R.layout.fragment_user, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        teGameId=view.findViewById(R.id.admin_id_by_user);
        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/ALGER.TTF");
        ((TextView)view.findViewById(R.id.game_start)).setTypeface(font);
        buttonStart=view.findViewById(R.id.game_start);
progress=view.findViewById(R.id.progressgame);
progress.setVisibility(View.INVISIBLE);
        user=view.findViewById(R.id.imageuser);
        Glide.with(getContext()).load(R.drawable.ui2).into(user);

        firebaseDatabase=FirebaseDatabase.getInstance();
        databaseReference=firebaseDatabase.getReference();
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                id=teGameId.getText().toString();
                progress.setVisibility(View.VISIBLE);
                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                query= databaseReference.child("users id");
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot datasearch:dataSnapshot.getChildren())
                        {
                            dataSearchId=datasearch.getValue(CreateAccount.Data.class);
                            if(dataSearchId.getId().equals(id))
                            {
                                if(dataSearchId.getOnValue().equals("On"))
                                {
                                    SharedPreferences sharedPreferences=getActivity().getPreferences(Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor=sharedPreferences.edit();
                                    editor.putString(getString(R.string.id_admin_user),id);
                                    editor.commit();
                                    progress.setVisibility(View.INVISIBLE);
                                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    ((MainActivity)getActivity()).changeFragment(12);

                                }
                                else{
                                    Toast.makeText(getActivity(),"Event not On",Toast.LENGTH_SHORT).show();
                                    progress.setVisibility(View.INVISIBLE);
                                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                }
                            }
                            else {
                                Toast.makeText(getActivity(), "Id doesn't exist", Toast.LENGTH_SHORT).show();
                                progress.setVisibility(View.INVISIBLE);
                                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });





            }
        });
    }
}
