package com.example.myapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class CustomAdapterClue extends RecyclerView.Adapter <CustomAdapterClue.MyViewHolder>{
    View view;
    Context context;
    LayoutInflater layoutInflater;
    DatabaseReference databaseReference;
    ClueList clueListFragment;

    ArrayList<AddingClue.Post> clueList ;


    public CustomAdapterClue(ClueList clueListFragment, Context context, ArrayList<AddingClue.Post>clueList){
        this.context=context;
        this.databaseReference=databaseReference;
        this.clueList=clueList;
        this.clueListFragment=clueListFragment;
        layoutInflater=LayoutInflater.from(context);
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view  =layoutInflater.inflate(R.layout.list_of_clue,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        holder.textView1.setText(clueList.get(position).getClue());
        holder.textView2.setText(clueList.get(position).getLat());
        holder.textView3.setText(clueList.get(position).getLon());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    clueListFragment.remove(clueList.get(position).getClue(),position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return clueList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textView1;
        TextView textView2;
        TextView textView3;
        ImageView imageView;
        public MyViewHolder(View itemView) {
            super(itemView);
            textView1=itemView.findViewById(R.id.cluelistext);
            textView2=itemView.findViewById(R.id.latlist);
            textView3=itemView.findViewById(R.id.lonlist);
            imageView=itemView.findViewById(R.id.cluecancel);
        }
    }
}
