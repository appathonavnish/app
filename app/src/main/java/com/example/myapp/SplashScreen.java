package com.example.myapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SplashScreen extends AppCompatActivity {
    ImageView imageView;
    final static  int SPLASH_TIME_OUT=3000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        imageView=findViewById(R.id.splash_image);
        Glide.with(getApplicationContext()).load(R.drawable.treasurehunt).into(imageView);
       /* firebaseAuth=FirebaseAuth.getInstance();
        firebaseUser=firebaseAuth.getCurrentUser();
        if(firebaseUser !=null) {
            Intent intent = new Intent(SplashScreen.this, MainActivity.class);
            intent.putExtra("check",1);
            startActivity(intent);
            finish();
        }*/


            Intent intent = new Intent(SplashScreen.this, Main2Activity.class);
            startActivity(intent);
            finish();



    }

}
