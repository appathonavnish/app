package com.example.myapp;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;


public class AddingTeam extends Fragment {
   TextInputEditText etTeamtitle;
   TextInputEditText etTotalMembers;
   TextInputEditText etVerification;
   Button buttonAddTeam;
   View view;
    ImageView imageView;
    ProgressBar progressBar;

    CustomProgressBar customProgressBar;

    FirebaseDatabase firebaseDatabaseTeam;
   DatabaseReference databaseReferenceTeam;
   FirebaseAuth firebaseAuth;
   FirebaseUser firebaseUser;
   Query query;

   String ettitle;
   String etmember;
   String etverify;

   //inflating the layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       view=inflater.inflate(R.layout.fragment_adding_team, container, false);
       return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"font/ALGER.TTF");
        ((TextView)view.findViewById(R.id.button_team_details)).setTypeface(font);
        progressBar=view.findViewById(R.id.progress_team_list);
        etTeamtitle=view.findViewById(R.id.team_title);
        etTotalMembers=view.findViewById(R.id.members);
        etVerification=view.findViewById(R.id.verification);
        buttonAddTeam=view.findViewById(R.id.button_team_details);
        imageView=view.findViewById(R.id.imageaddingteam);
        Glide.with(getContext()).load(R.drawable.ui2).into(imageView);

        customProgressBar=new CustomProgressBar(getActivity(),progressBar);
        customProgressBar.hideProgressBar();

        firebaseDatabaseTeam=FirebaseDatabase.getInstance();
        databaseReferenceTeam=firebaseDatabaseTeam.getReference();
        firebaseAuth=FirebaseAuth.getInstance();
        firebaseUser=firebaseAuth.getCurrentUser();

        //submitting the button to team
        buttonAddTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ettitle=etTeamtitle.getText().toString();
                etmember=etTotalMembers.getText().toString();
                etverify=etVerification.getText().toString();

                if(ettitle.isEmpty()||etmember.isEmpty()||etverify.isEmpty())
                {
                    final CustomDialog customDialog =new CustomDialog();
                    customDialog.message("Enter all the fields", "Ok", null, new CustomInterfaceDialog() {
                        @Override
                        public void onPositiveButtonClicked() {
                            customDialog.dismiss();
                        }

                        @Override
                        public void onNegativeButtonClicked() {

                        }

                        @Override
                        public void onCancelButtonClicked() {

                        }
                    });
                    customDialog.show(getFragmentManager(),"Dialog");
                }
                else { customProgressBar.showProgressBar();
                    writeDatabase();
                    customProgressBar.hideProgressBar();

                }
            }
        });

    }
    public void writeDatabase()
    {   //query to search for admin id
        query=databaseReferenceTeam.child("users id").child(firebaseUser.getUid()).child("id");
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String adminId=dataSnapshot.getValue().toString();

                AddingTeam.Team team=new AddingTeam.Team();
                team.setTeamTitle(ettitle);
                team.setTotalMembers(etmember);
                team.setTeamVerification(etverify);
                team.setDone("0");
                team.setTimeTaken("0");
                team.setStartTime("00:00:00");
                team.setEndTime("00:00:00");
                HashMap<String,Object>map=team.mapping();

                HashMap<String,Object>mapped=new HashMap<>();
                mapped.put("/Teams/"+adminId+"/"+team.getteamTitle(),map);
                databaseReferenceTeam.updateChildren(mapped).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getActivity(),"Team Added",Toast.LENGTH_SHORT).show();
                        customProgressBar.hideProgressBar();
                        ((MainActivity)getActivity()).changeFragment(3);
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    public static class Team
    {
        private String teamTitle;
        private String totalMembers;
        private String  teamVerification;
        private String  startTime;
        private String endTime;
        private String timeTaken;
        private String done;

        public String getStartTime() {
            return startTime;
        }
        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }
        public String getEndTime() {
            return endTime;
        }
        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }
        public String getTimeTaken() {
            return timeTaken;
        }
        public void setTimeTaken(String timeTaken) {
            this.timeTaken = timeTaken;
        }
        public String getDone() {
            return done;
        }
        public void setDone(String done) {
            this.done = done;
        }
        /* public Team(String title,String members,String verfication)
                {
                    teamTitle=title;
                    totalMembers=members;
                    teamVerification=verfication;
                }*/
        public void setTeamTitle(String teamTitle ){ this.teamTitle=teamTitle;}
        public void setTotalMembers(String totalMembers){ this.totalMembers=totalMembers;}
        public void setTeamVerification(String teamVerification){ this.teamVerification=teamVerification;}



        public String getteamTitle()
         {
             return teamTitle;
         }
        public String getTotalMembers()
        {
            return totalMembers;
        }
        public String getTeamVerification()
        {
            return teamVerification;
        }
        public HashMap mapping()
        {
            HashMap<String,Object> team=new HashMap<>();
            team.put("teamTitle",teamTitle);
            team.put("totalMembers",totalMembers);
            team.put("teamVerification",teamVerification);
            team.put("done",done);
            team.put("startTime",startTime);
            team.put("endTime",endTime);
            team.put("timeTaken",timeTaken);
            return team;
        }
    }

}
