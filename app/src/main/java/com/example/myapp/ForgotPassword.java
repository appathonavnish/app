package com.example.myapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;


public class ForgotPassword extends Fragment {

    View view;
    FirebaseAuth firebaseAuth;
    TextInputEditText teEmail;
    Button button;
    ImageView forgot;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =inflater.inflate(R.layout.fragment_forgot_password, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        teEmail=view.findViewById(R.id.reset_email);
        button =view.findViewById(R.id.send_email);
        firebaseAuth=FirebaseAuth.getInstance();
        forgot=view.findViewById(R.id.forgot);
        Glide.with(getContext()).load(R.drawable.ui2).into(forgot);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(teEmail.toString().isEmpty())
                {
                    Toast.makeText(getActivity(),"Cannot be Empty",Toast.LENGTH_SHORT).show();
                }
                else {
                    firebaseAuth.sendPasswordResetEmail(teEmail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(getActivity(), "Email sent", Toast.LENGTH_SHORT).show();
                            ((MainActivity) getActivity()).changeFragment(0);
                        }
                    });
                }
            }
        });


    }
}
