package com.example.myapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

public class CustomDialog extends DialogFragment {
     AlertDialog.Builder alertDialog;
   String message="Hello",positiveTitle = "ok",negativeTitle="cancel";
   CustomInterfaceDialog customInterfaceDialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton(positiveTitle, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                customInterfaceDialog.onPositiveButtonClicked();
            }
        });
        alertDialog.setCancelable(true);


        return alertDialog.create();
    }


   public void message(String mess,String positiveTitle,@Nullable String negativeTitle, CustomInterfaceDialog customInterfaceDialog)
   {
       message=mess;
       this.positiveTitle = positiveTitle;
       this.negativeTitle = negativeTitle;
       this.customInterfaceDialog = customInterfaceDialog;
   }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }

}

